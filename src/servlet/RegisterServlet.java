package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import logic.UserLogic;


@WebServlet("/RegisterServlet")
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//セッションスコープを使う
		HttpSession session = request.getSession();

		//actionリクエストパラメータの取得
				String action = request.getParameter("action");

				System.out.println(action);

				if(action == null) {

					//userRegister.jspへフォワード
					String path = "/userRegister.jsp";
					RequestDispatcher dis = request.getRequestDispatcher(path);
					dis.forward(request, response);

				}else if(action.equals("done")){



					//セッションスコープの情報を取得する
					User registerUser = (User)session.getAttribute("registerUser");

					//登録処理を行う
					UserLogic logic = new UserLogic();
					boolean res = logic.execute(registerUser);
					if (res == false) {
						//registerError.jspへフォワード
						String path = "/userError.jsp";
						RequestDispatcher dis = request.getRequestDispatcher(path);
						dis.forward(request, response);
					}
//					logic.execute(registerUser);

					//セッションスコープのインスタンスを削除
//					session.removeAttribute("registerUser");

					//userDone.jspへフォワード
					String path = "/userDone.jsp";
					RequestDispatcher dis = request.getRequestDispatcher(path);
					dis.forward(request, response);
				}
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//文字コードの指定
				request.setCharacterEncoding("utf-8");

				//userRegister.jspからリクエストパラメータの取得
				String loginId = request.getParameter("loginid");
				String passward = request.getParameter("passward");
				String name = request.getParameter("name");
				String branch = request.getParameter("branch");
				String department = request.getParameter("department");




				//ユーザー情報を保持するインスタンス
				User registerUser = new User(loginId,passward,name,branch,department);

				//セッションスコープを使う
				HttpSession session = request.getSession();

				//セッションスコープに情報を保持する
				session.setAttribute("registerUser", registerUser);

				//registerConfirm.jspへフォワード
				String path = "/userConfirm.jsp";
				RequestDispatcher dis = request.getRequestDispatcher(path);
				dis.forward(request, response);

	}

}
