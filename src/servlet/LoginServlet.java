package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import logic.LoginLogic;

@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


	}//doGet

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//リクエストパラメーターの取得（ログインの際のニックネーム、パスワードを受け取る）
		request.setCharacterEncoding("utf-8");
		String login_id = request.getParameter("login_id");
		String passward = request.getParameter("passward");

		//ユーザー情報入手できた場合
				User loginUser = new User(login_id,passward);
				User account = new User();
				LoginLogic ll = new LoginLogic();
				account = ll.execute(loginUser);

				if(account !=null) {
					HttpSession session = request.getSession();
					session.setAttribute("accountUser", account);

					//main画面へフォワード
					String path ="/main.jsp";
					RequestDispatcher dis = request.getRequestDispatcher(path);
					dis.forward(request, response);

				}else {
					//ユーザー情報が入手できなかった場合
					//ログインエラー画面へ移動
					String path ="/error.jsp";
					RequestDispatcher dis = request.getRequestDispatcher(path);
					dis.forward(request, response);
				}


	}//doPost

}//class
