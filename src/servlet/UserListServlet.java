package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import logic.UserListLogic;

/**
 * Servlet implementation class OfferCheckServlet
 */
@WebServlet("/UserListServlet")
public class UserListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//セッションスコープを使う
		HttpSession session = request.getSession();
		User user = (User)session.getAttribute("User");
		int id = user.getId();

		UserListLogic ul = new UserListLogic();
		List<User> userList = ul.execute(id);

		session.setAttribute("userList", userList);

		//plantSearch.jspへフォワード
		String path ="/index.jsp";
		RequestDispatcher dis = request.getRequestDispatcher(path);
		dis.forward(request, response);

	}//doGet()

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}//doPost()

}
