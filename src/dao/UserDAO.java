package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.User;
import exception.SQLRuntimeException;

public class UserDAO {


	public boolean insert(Connection connection, User user) {

		//データベースの接続
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");

			//SQL文を作成（insert）
			String sql = "INSERT INTO USER_TB(login_id,passward,name,branch_id,department_id) VALUES(?,?,?,?,?)";

			//sqlを送信する
			PreparedStatement pst = connection.prepareStatement(sql.toString());


			pst.setString(1, user.getLogin_id());
			pst.setString(2, user.getPassward());
			pst.setString(3, user.getName());
			pst.setString(4, user.getBranch());
			pst.setString(5, user.getDepartment());


			//sqlの実行結果を取得
			pst.executeUpdate();

		} catch (SQLException |ClassNotFoundException e) {
			// 接続エラー時はコンソールに出力
			e.printStackTrace();
			return false;
		}
		return true;

	}//insert()

	public void stopped(Connection connection, User user) {

		PreparedStatement pst = null;

		//データベースの接続
		try {

			//SQL文を作成（update）
			String sql = "";

			//sqlを送信する
			 pst = connection.prepareStatement(sql);


			pst.setInt(1, user.getId());

			//sqlの実行結果を取得
			int rs = pst.executeUpdate();

			System.out.println(rs);

			//sqlの実行結果を取得
			pst.executeUpdate();

		} catch (SQLException e) {
			// 接続エラー時はコンソールに出力
			e.printStackTrace();
			throw new SQLRuntimeException(e);
		}finally {
		close(pst);
		}
	}//delete()



}//class
