package dao;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {
	private static final String DRIVER_NAME = "com.mysql.cj.jdbc.Driver";
	private static final String CONNECT_URL = "jdbc:mysql://localhost:3306/miyagawa_kentaro?characterEncoding=utf8&serverTimezone=GMT%2B9";
	private static final String USER_NAME = "root";
	private static final String PASSWORD = "root";

	private Connection con;

	public Connection getConnect() throws ClassNotFoundException,SQLException{
		if (this.con == null) {
			Class.forName(DRIVER_NAME);
			this.con = DriverManager.getConnection(CONNECT_URL, USER_NAME, PASSWORD);
		}

		return this.con;
	}

	public void closeConnect() throws Exception {
		if (this.con != null) {
			this.con.close();
			this.con = null;
		}
	}
}
