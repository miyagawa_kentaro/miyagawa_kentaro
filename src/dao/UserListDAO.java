package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import beans.User;

public class UserListDAO {
	private DBConnection db;

	public UserListDAO(DBConnection db) {
		this.db = db;
	}

	public List<User> OfferCheckSelect(int id) {
		Connection con = null;
		PreparedStatement stmt = null;
		User user = null;
		List<User> userList = new ArrayList<>();

		try {
			con = db.getConnect();

			String sql ="select id,login_id,passward,name,branch_id,department_id,"
					+ "is_stopped,created_date,updated_date from user;";

			//SQLを送る
			PreparedStatement pst = con.prepareStatement(sql);

			//SQLの？に値をセット
			pst.setInt(1, id);

			//SQLを実行して結果を取得
			ResultSet rs = pst.executeQuery();

			//結果を繰り返し取得する
			while (rs.next()) {

				int uid  =rs.getInt("id");
				String loginId =rs.getString("login_id");
				String passward = rs.getString("passward");
				String name =rs.getString("name");
				String branchId = rs.getString("branch_id");
				String departmentId = rs.getString("department_id");
				int isStopped =rs.getInt("isStopped");
				Date created =rs.getDate("created_date");
				Date updated =rs.getDate("updated_date");

				user =  new User(uid,loginId,passward,name,branchId,departmentId,isStopped,created,updated);

				userList.add(user);

			}


		}catch(Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
			return null;
		}

		return userList;

	}//OfferCheckSelect()

}//class
