package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import beans.User;



public class LoginUserDAO {

	private DBConnection db;

	public LoginUserDAO(DBConnection db) {
		this.db = db;
	}

	public User loginSelect(User loginuser) {
		Connection con = null;
		PreparedStatement stmt = null;
		User account = null;

		try {
			con = db.getConnect();

			String sql ="SELECT * FROM users WHERE login_id=? AND passward=?";

			//SQLを送る
			PreparedStatement pst = con.prepareStatement(sql);

			//SQLの？に値をセット
			pst.setString(1, loginuser.getLogin_id());
			pst.setString(2, loginuser.getPassward());

			//SQLを実行して結果を取得
			ResultSet rs = pst.executeQuery();

			//結果を繰り返し取得する
			while (rs.next()) {

				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String passward = rs.getString("passward");
				String name = rs.getString("name");
				String branch = rs.getString("branch");
				String department = rs.getString("department");


				//アカウントを管理するインスタンス
				account  = new User(loginId,passward,name,branch,department);

			}


		}catch(Exception e) {
			//System.out.println(e.getMessage());
			e.printStackTrace();
			return null;
		}

		return account;

	}//loginSelect()



}//class
