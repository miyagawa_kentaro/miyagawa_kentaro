package logic;

import beans.User;
import dao.DBConnection;
import dao.LoginUserDAO;

public class LoginLogic {
	public User execute(User user) {

		DBConnection db = new DBConnection();
		LoginUserDAO dao = new LoginUserDAO(db);
		User account = dao.loginSelect(user);
		return account;

	}//execute()

}
