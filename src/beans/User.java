package beans;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable{

	private int id;
	private String login_id;
	private String passward;
	private String name;
	private String branch;
	private String department;

	private int  isStopped;
	private Date createdDate;
    private Date updatedDate;


    public User() {

	}

	public User(String login_id,String passward) {
		this.login_id=login_id;
		this.passward=passward;

	}

	public User(int id) {
		this.id=id;
	}

	public User(Date createdDate,Date updatedDate) {
		this.createdDate=createdDate;
		this.updatedDate=updatedDate;

	}

	public User(String login_id,String passward,String name,String branch,
			String department)

	{


		this.login_id=login_id;
		this.passward=passward;
		this.name = name;
		this.branch = branch;
		this.department=department;
//		this.createdDate=createdDate;
//		this.updatedDate=updatedDate;

	}

	public User(int id,String login_id,String passward,String name,String branch,
			String department,int isStopped,Date createdDate,Date updatedDate)
	{
		this.id=id;
		this.login_id=login_id;
		this.passward=passward;
		this.name = name;
		this.branch = branch;
		this.department=department;
		this.isStopped=isStopped;
		this.createdDate=createdDate;
		this.updatedDate=updatedDate;

	}


	public int getIsStopped() {
		return isStopped;
	}

	public void setIsStopped(int isStopped) {
		this.isStopped = isStopped;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin_id() {
		return login_id;
	}

	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}

	public String getPassward() {
		return passward;
	}

	public void setPassward(String passward) {
		this.passward = passward;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}


}
