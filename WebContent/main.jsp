<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import= "beans.User"%>
<%@ page import = "java.util.List" %>
<%List<User> userList = (List<User>) session.getAttribute("userlist"); %>
<% User user = (User)session.getAttribute("User"); %>
<!DOCTYPE html>
<html>
<head>

<title>ホーム画面</title>
</head>
<body>


<p>ユーザー情報</p>
<%
for(User list : userList ){
%>
<ul>
<li>ID:<%=user.getId() %></li>
<li>LOGIN ID:<%=user.getLogin_id() %></li>
<li>PASSWARD:<%=user.getPassward() %></li>
<li>NAME:<%=user.getName() %></li>
<li>BRANCH ID:<%=user.getBranch() %></li>
<li>DEPARTMENT ID:<%=user.getDepartment()%></li>
<li>IS STOPPED:<%=user.getIsStopped() %></li>
<li>CREATED:<%=user.getCreatedDate() %></li>
<li>UPDARED:<%=user.getUpdatedDate()%></li>
</ul>
<%} %>
<p><a href="userRegister.jsp">新規ユーザー登録</a></p>

<p><a href="userUpdate.jsp">ユーザー編集</a></p>


<a href="logoutConfirm.jsp">LOGOUT</a>
</body>
</html>