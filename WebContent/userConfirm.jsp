<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
     <%@ page import="beans.User" %>
    <%
    User registerUser = (User)session.getAttribute("registerUser");
    %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>確認画面</title>
</head>
<body>
<h1>ユーザー登録</h1>
<h2>確認画面</h2>

<ul>
	
	<li>ログインID:<%=registerUser.getLogin_id()%></li>
	<li>パスワード:<%=registerUser.getPassward()%></li>
	<li>名称:<%=registerUser.getName() %></li>
	<li>支店:<%=registerUser.getBranch()%></li>
	<li>部署:<%=registerUser.getDepartment() %></li>
</ul>
<!-- 登録ボタン -->
<p><a href="/miyagawa_kentaro/RegisterServlet?action=done">登録</a></p>
<a href="./userRegister.jsp">戻る</a>
</body>
</html>